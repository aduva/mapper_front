import {BrowserModule} from '@angular/platform-browser';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import {NgModule} from '@angular/core';

import {RouterModule, Router} from '@angular/router';

import {AppComponent} from './app.component';
import {AppRouterModule} from './app.router';
import {FormsModule} from '@angular/forms';
import {HttpModule, XSRFStrategy, CookieXSRFStrategy, Http, ConnectionBackend, XHRBackend, RequestOptions} from '@angular/http';
import {AdminModule} from './admin/admin.module';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        AppRouterModule,
        FormsModule,
        AdminModule
    ],
    declarations: [
        AppComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
