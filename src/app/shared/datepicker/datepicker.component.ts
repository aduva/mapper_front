import {Component, Input, Output, EventEmitter, OnChanges, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgModule} from '@angular/core';
import {NgxMyDatePickerModule} from 'ngx-mydatepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

import * as moment from 'moment/moment';

@Component({
    selector: 'datepicker',
    template: `
        <div class="input-group">
            <span class="input-group-addon" (click)="dp.toggleCalendar();$event.stopPropagation()"><i class="fa fa-calendar"></i></span>
            <input class="{{classes}}" placeholder="{{placeholder}}" ngx-mydatepicker [(ngModel)]="model"
                [options]="options" #dp="ngx-mydatepicker" (dateChanged)="changed($event)"
                (click)="dp.toggleCalendar();$event.stopPropagation()"/>
            <span class="input-group-addon" (click)="dp.clearDate();$event.stopPropagation()"><i class="fa fa-times"></i></span>
        </div>
    `
})
export class DatepickerComponent implements OnChanges {
    @Input('value') value: any;
    @Input('classes') classes = 'form-control';
    @Input('locale') locale? = 'ru';
    @Input('placeholder') placeholder = 'Выберите дату';
    @Input('dateFormat') dateFormat = 'dd.mm.yyyy';
    @Input('options') options: any;

    @Output('onValueChanged') valueChanged: EventEmitter<any> = new EventEmitter();
    @ViewChild('dp') dp: any;
    // Initialized to specific date (09.10.2018)
    private model: Object;

    ngOnChanges(changes) {
        if (changes.value && changes.value.currentValue) {
            const date: moment.Moment = moment(changes.value.currentValue);
            this.model = ({ date: { year: date.year(), month: date.month() + 1, day: date.date() }});
        } else if (changes.value && !changes.value.currentValue) {
            this.model = undefined;
            this.dp.clearDate();
        }

        if (changes.options && changes.options.currentValue) {
            this.options = changes.options.currentValue;
        } else {
            this.options  = {
                dateFormat: this.dateFormat,
                locale: 'ru'
            };
        }
    }

    changed(event) {
        if (this.model) {
            this.valueChanged.emit(event.jsdate);
        }
    }
}

@NgModule({
    imports: [CommonModule, FormsModule, NgxMyDatePickerModule],
    declarations: [DatepickerComponent],
    exports: [NgxMyDatePickerModule, DatepickerComponent]
})
export class DatepickerModule { }