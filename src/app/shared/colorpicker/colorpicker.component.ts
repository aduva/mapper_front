import {Component, OnInit, Input, Output, EventEmitter, OnChanges, ElementRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Http, Headers} from '@angular/http';
import {NgModule}       from '@angular/core';
import {FormsModule, ReactiveFormsModule}    from '@angular/forms';
import {CommonModule}   from '@angular/common';
import {ColorPickerModule} from 'angular2-color-picker';
import * as moment from 'moment/moment';

@Component({
    selector: 'colorpicker',
    template: `
        <div class="input-group">
            <div class="input-group-addon">
                <i [style.background-color]="value"></i>
            </div>
            <input [colorPicker]="value" #cp class="form-control" (colorPickerChange)="changed($event)" [value]="value" [cpPosition]="'bottom'">
        </div>
    `
})
export class ColorpickerComponent {
    @Input('value') value: any;
    @Input('classes') classes: string = 'form-control';
    @Input('locale') locale?: string = 'ru';
    @Input('placeholder') placeholder: string = 'Выберите цвет';
    @Input('dateFormat') dateFormat: string = 'dd.mm.yyyy';
    @Input('options') options: any;
    @Output('onValueChanged') valueChanged = new EventEmitter();
    // @ViewChild('cp') cp: ElementRef;

    // Initialized to specific date (09.10.2018)
    private model: Object;

    changed(event) {
        this.value = event;
        this.valueChanged.emit(this.value);
    }
}

@NgModule({
    imports: [CommonModule, FormsModule, ColorPickerModule],
    declarations: [ColorpickerComponent],
    exports: [ColorpickerComponent, ColorPickerModule]
})
export class ColorpickerModule {}