import {NgModule, ModuleWithProviders} from '@angular/core';
import {InMemoryWebApiModule} from 'angular2-in-memory-web-api';
import {InMemoryDbService} from 'angular2-in-memory-web-api';

export class InMemoryService implements InMemoryDbService {
	createDb() {
		let reports = [
			{ id: 1, title: 'Учет откатов' },
			{ id: 2, title: 'Отмытые финансы' },
			{ id: 3, title: 'Офшорные счета' }
		];
		let columns = [
			{ id: 1, title: 'COL_1' },
			{ id: 2, title: 'COL_2' },
			{ id: 3, title: 'COL_3' }
		];
		let rows = [
			{ id: 1, title: 'ROW_1' },
			{ id: 2, title: 'ROW_2' },
			{ id: 3, title: 'ROW_3' }
		];
		let values = [
			{ id: 1, title: 'VAL_1' },
			{ id: 2, title: 'VAL_2' },
			{ id: 3, title: 'VAL_3' }
		];
		let indexes = [
			{ id: 1, title: 'Сумма', column_id: 1, row_id: 1, report_id: 1 },
			{ id: 2, title: 'Процент с тендера', column_id: 2, row_id: 2, report_id: 1 },
			{ id: 3, title: 'Процент для шефа', column_id: 3, row_id: 3, report_id: 1 },
			{ id: 4, title: 'Шапка наверх', column_id: 4, row_id: 4, report_id: 1 },
			{ id: 5, title: 'Откат', column_id: 5, row_id: 5, report_id: 2 },
			{ id: 6, title: 'Благодарность', column_id: 6, row_id: 6, report_id: 2 },
			{ id: 7, title: 'Бонус за лояльность', column_id: 7, row_id: 7, report_id: 3 },
			{ id: 8, title: 'Отступные', column_id: 8, row_id: 8, report_id: 3 },
			{ id: 9, title: 'Неприкосновенный запас', column_id: 9, row_id: 9, report_id: 3 },
		];
		let mappings = [
			{
				id: 1,
				title: 'Общая сумма',
				mapped_indexes: [
					{index: { id: 1, title: 'Сумма', report_id: 1 }, dateStart: '2016-05-01', dateEnd: '2016-07-01', color: '#FFB6C1'},
					{index: { id: 2, title: 'Процент с тендера', report_id: 1 }, dateStart: '2016-08-01', dateEnd: '2016-10-01', color: '#ADD8E6'},
					{index: { id: 4, title: 'Шапка наверх', report_id: 1 }, dateStart: '2016-10-01', dateEnd: '', color: '#90EE90'}
				]
			},
			{
				id: 2,
				title: 'Процент',
				mapped_indexes: [
					{index: { id: 2, title: 'Процент с тендера', report_id: 1 }, dateStart: '2016-06-01', dateEnd: '2017-01-01', color: '#FFB6C1'},
					{index: { id: 3, title: 'Процент для шефа', report_id: 1 }, dateStart: '2017-02-01', dateEnd: '', color: '#ADD8E6'}
				]
			},
			{
				id: 3,
				title: 'Шапка вышестоящим',
				mapped_indexes: [
					{index: { id: 3, title: 'Процент для шефа', report_id: 1 }, dateStart: '2016-07-01', dateEnd: '2016-09-01', color: '#FFB6C1'},
					{index: { id: 4, title: 'Шапка наверх', report_id: 1 }, dateStart: '2016-10-01', dateEnd: '', color: '#ADD8E6'}
				]
			}
		];

		return {reports, columns, rows, values, indexes, mappings};
	}
}

@NgModule({
  imports: [
    InMemoryWebApiModule.forRoot(InMemoryService)
  ],
  exports: [
    InMemoryWebApiModule
  ]
})

export class AdminApiModule {}
