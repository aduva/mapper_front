import {ReportsComponent} from './reports/reports.component';
import {ReportsDetailsComponent} from './reports/reports-details.component';
import {ReportsService} from './reports/reports.service';
import {reports, selectedReport, indexes} from './reports/reports.store';

import {MappingComponent} from './mapping/mapping.component';
import {SelectedMappingComponent} from './mapping/selected-mapping.component';
import {MappedIndexComponent} from './mapping/mapped-index.component';
import {IndexesComponent} from './mapping/indexes.component';
import {IndexItemComponent} from './mapping/index-item.component';
import {AddMappingComponent} from './mapping/add-mapping.component';
import {MappingService} from './mapping/mapping.service';
import {mappings, selectedMappedIndex, selectedMapping, mappedIndexes, addMapping} from './mapping/mapping.store';

import {AdminHomeComponent} from './admin.component';

export const COMPONENTS = [
    AdminHomeComponent,
    MappingComponent,
	SelectedMappingComponent,
    MappedIndexComponent,
	AddMappingComponent,
	ReportsComponent,
	ReportsDetailsComponent,
    IndexesComponent,
    IndexItemComponent
];

export const SERVICES = [
	MappingService,
	ReportsService
];

export const STORES = {
	reports, selectedReport, indexes,
    mappings, selectedMappedIndex, selectedMapping, mappedIndexes, addMapping
};