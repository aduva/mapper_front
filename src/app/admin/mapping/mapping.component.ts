import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MappingService} from './mapping.service';
import {MappingStore} from './mapping.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

import * as moment from 'moment/moment';
import 'moment/locale/ru';

@Component({
	moduleId: module.id,
	selector: 'mapping',
	templateUrl: 'mapping.component.html'
})
export class MappingComponent implements OnInit {
	reports: Observable<Array<any>>;
	mappings: Observable<Array<any>>;

	selectedReport: Observable<any>;
	selectedMapping: Observable<any>;
	searchText: string = '';

	addMapping: Observable<any>;

	constructor(private service: MappingService) {
		this.reports = service.reports;
		this.mappings = service.mappings;
		this.selectedMapping = service.selectedMapping;
		this.selectedReport = service.selectedReport;
		this.addMapping = service.addMapping;

		moment.locale('ru');
	}

	ngOnInit() {
		this.changed();
	}

	changed() {
		this.search(this.searchText);
	}

	search(term: string) {
		this.service.searchMappings(term);
	}

	select(mapping) {
		this.service.selectMapping(mapping);
	}

	add() {
		this.service.setAddMapping({value: true});
	}
}
