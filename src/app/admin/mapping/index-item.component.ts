import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MappingService} from './mapping.service';
import {MappingStore} from './mapping.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';

import * as moment from 'moment/moment';
import 'moment/locale/ru';

@Component({
	moduleId: module.id,
	selector: 'index-item',
	templateUrl: 'index-item.component.html'
})
export class IndexItemComponent implements OnInit {
	@Input() item: any;
    mappedIndexes: Observable<Array<any>>;
    selectedMappedIndex: Observable<any>;
    color: string = '#fff';
	constructor(private service: MappingService) {
        this.mappedIndexes = service.mappedIndexes;
        this.selectedMappedIndex = service.selectedMappedIndex;
	}

    ngOnInit() {
        this.mappedIndexes.subscribe(v => {
            console.log(this.item);
            this.color = '#fff';
            if (!this.item || !v)
                return;

            let itm = v.find(el => el.parents.indices[0].pokaz_sid === this.item.pokaz_sid);
            if (itm) {
                this.color = itm.color;
            }
        });

        this.selectedMappedIndex.subscribe(v => {
            if (this.item && v && v.parents.indices[0].pokaz_sid === this.item.pokaz_sid) {
                this.color = v.color;
            }
        });
    }
}