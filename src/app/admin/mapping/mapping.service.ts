import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Http, Headers}    from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MappingStore} from './mapping.store';
import {ReportsService} from '../reports/reports.service';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class MappingService {
    reports: Observable<Array<any>>;
    selectedReport: Observable<any>;
    selectedMapping: Observable<any>;
	selectedMappedIndex: Observable<any>;
    indexes: Observable<Array<any>>;
    mappedIndexes: Observable<Array<any>>;
    mappings: Observable<Array<any>>;
    addMapping: Observable<any>;
    url: string = 'api/mapping/';
    mappingsLength: number = 3;
    private searchMappingsStream = new Subject<string>();
    private searchIndexesStream = new Subject<string>();

    constructor(private http: Http,
                private store: Store<MappingStore>,
                private reportsService: ReportsService) {
    	this.reports = reportsService.items;
        this.selectedReport = reportsService.item;
        this.indexes = reportsService.indexes;

        this.mappings = store.select('mappings');
        this.addMapping = store.select('addMapping');
        this.selectedMapping = store.select('selectedMapping');
        this.selectedMappedIndex = store.select('selectedMappedIndex');
        this.mappedIndexes = store.select('mappedIndexes');

        this.mappings.subscribe(v => this.mappingsLength = v.length);

    	this.searchMappingsStream
			.debounceTime(300)
			.distinctUntilChanged()
			.subscribe(term => this.fetchMappings(term));

        this.searchIndexesStream
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(term => this.fetchIndexes(term));
    }

    fetchReports(term: string) {
    	this.reportsService.fetch(term);
    }

    fetchMappings(term) {
        this.http.get('/api/global_indexes?term=' + term)
            .map(res => res.json().data.content)
            .map(payload => ({ type: 'ADD_MAPPINGS', payload: payload }))
            .subscribe(action => this.store.dispatch(action))
        ;
    }

    fetchMappedIndexes(global_index_sid) {
        this.http.get('/api/mappings?include=indexes&global_index_sid=' + global_index_sid)
            .map(res => res.json().data)
            .subscribe(items => this.addMappedIndexes(items))
        ;
    }

    fetchIndexes(term) {
        this.reportsService.fetchIndexes(term);
    }

    fetchOne(id) {
    	// this.http.get(this.url + id)
		   //  .map(res => res.json().data)
		   //  .subscribe(item => this.selectReport(item))
     //    ;
    }

    searchMappings(term: string) {
    	this.searchMappingsStream.next(term);
    }

    searchIndexes(term: string) {
        this.searchIndexesStream.next(term);
    }

    addNewMapping(item) {
        this.store.dispatch({type: 'ADD_MAPPING', payload: item});
    }

    selectMapping(item) {
        console.log(item);
        this.store.dispatch({type: 'SELECT_MAPPING', payload: item});
        this.fetchMappedIndexes(item.global_index_sid);
        // this.selectMappedIndex(item.mapped_indexes && item.mapped_indexes.length > 0 ? item.mapped_indexes[0] : undefined);
        // this.addMappedIndexes(item.mapped_indexes);
    }

    addMappedIndexes(items) {
        this.store.dispatch({type: 'ADD_MAPPED_INDEXES', payload: items});
    }

    addMappedIndex(item) {
        this.store.dispatch({type: 'ADD_MAPPED_INDEX', payload: item});
    }

    selectMappedIndex(item) {
        this.store.dispatch({type: 'SELECT_MAPPED_INDEX', payload: item});
    }

    updateMappedIndex(item) {
        this.store.dispatch({type: 'UPDATE_MAPPED_INDEX', payload: item});
    }

    deleteMappedIndex(item) {
        this.store.dispatch({type: 'DELETE_MAPPED_INDEX', payload: item});
    }

    updateMapping(item) {
        this.store.dispatch({type: 'UPDATE_MAPPING', payload: item});
        this.addMappedIndexes(item.mapped_indexes);
    }

    deleteMapping(item) {
        this.store.dispatch({type: 'DELETE_MAPPING', payload: item});
        this.store.dispatch({type: 'SELECT_MAPPING', payload: undefined});
        this.store.dispatch({type: 'SELECT_MAPPED_INDEX', payload: undefined});
        this.store.dispatch({type: 'ADD_MAPPED_INDEXES', payload: undefined});
    }

    setAddMapping(value) {
        this.store.dispatch({type: 'SET_ADD_MAPPING', payload: value});
    }
}