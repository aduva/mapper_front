import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Component, OnInit, Input} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MappingService} from './mapping.service';
import {MappingStore} from './mapping.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';

import * as moment from 'moment/moment';
import 'moment/locale/ru';

@Component({
	moduleId: module.id,
	selector: 'mapped-index',
	templateUrl: 'mapped-index.component.html'
})
export class MappedIndexComponent {
	@Input() item: any;
	options: any;
	colors = [
		{id: 1, name: 'Розовый', code: '#FFB6C1'},
		{id: 2, name: 'Красный', code: '#F08080'},
		{id: 3, name: 'Бордовый', code: '#CD5C5C'},
		{id: 4, name: 'Голубой', code: '#ADD8E6'},
		{id: 5, name: 'Синий', code: '#00BFFF'},
		{id: 6, name: 'Фиолетовый', code: '#9370DB'},
		{id: 7, name: 'Зеленый', code: '#90EE90'},
		{id: 8, name: 'Бежевый', code: '#F5DEB3'},
		{id: 9, name: 'Оранжевый', code: '#FFA07A'},
		{id: 10, name: 'Желтый', code: '#FFD700'}
	];

	constructor(private service: MappingService) {
	}

	startChanged(event) {
		let date: string = '';
		if (event) {
			date = moment(event).format('YYYY-MM-DD');
		}
		this.update(Object.assign({}, this.item, {dateStart: date}));
	}

	endChanged(event) {
		let date: string = '';
		if (event) {
			date = moment(event).format('YYYY-MM-DD');
		}
		// this.item.dateEnd = date;
		this.update(Object.assign({}, this.item, {dateEnd: date}));

	}

	colorChanged(event) {
		// item.color=$event;
		this.update(Object.assign({}, this.item, {color: event}));
	}

	update(value) {
		this.service.updateMappedIndex(value);
	}

	delete() {
		this.service.deleteMappedIndex(this.item);
		this.service.selectMappedIndex(undefined);
	}
}