export interface MappingStore {
	mappings: any[];
	selectedMappedIndex: any;
    selectedMapping: any;
	addMapping: any;
    indexes: any[];
	mappedIndexes: any[];
};

export const mappings = (state: any = [], {type, payload}) => {
  	switch (type) {
  		case 'ADD_MAPPINGS':
	    	return payload;
        case 'ADD_MAPPING':
            return [...state, payload];
        case 'UPDATE_MAPPING':
            return state.map(item => {
                return item.id === payload.id ? Object.assign(item, payload) : item;
            });
        case 'DELETE_MAPPING':
            return state.filter(item => {
                return item.id !== payload.id;
            });
	    default:
	      	return state;
  	}
};

export const indexes = (state: any = [], {type, payload}) => {
  	switch (type) {
  		case 'ADD_INDEXES':
	    	return payload;
	    default:
	      	return state;
  	}
};

export const mappedIndexes = (state: any = [], {type, payload}) => {
    switch (type) {
        case 'ADD_MAPPED_INDEXES':
            return payload;
        case 'ADD_MAPPED_INDEX':
            return [...state, payload];
        case 'UPDATE_MAPPED_INDEX':
            return state.map(item => {
                return item.index.id === payload.index.id ? Object.assign(item, payload) : item;
            });
        case 'DELETE_MAPPED_INDEX':
            return state.filter(item => {
                return item.index.id !== payload.index.id;
            });
        default:
            return state;
    }
};

export const selectedMappedIndex = (state: any = null, {type, payload}) => {
	switch (type) {
		case 'SELECT_MAPPED_INDEX':
			return payload;
		default:
			return state;
	}
};

export const selectedMapping = (state: any = null, {type, payload}) => {
	switch (type) {
		case 'SELECT_MAPPING':
			return payload;
		default:
			return state;
	}
};

export const addMapping = (state: any = null, {type, payload}) => {
    switch (type) {
        case 'SET_ADD_MAPPING':
            return payload;
        default:
            return state;
    }
};