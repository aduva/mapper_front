import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MappingService} from './mapping.service';
import {MappingStore} from './mapping.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';

import * as moment from 'moment/moment';
import 'moment/locale/ru';

@Component({
	moduleId: module.id,
	selector: 'indexes',
	templateUrl: 'indexes.component.html'
})
export class IndexesComponent {
	indexes: Observable<Array<any>>;
    searchText: string = '';

	constructor(private service: MappingService) {
        this.indexes = service.indexes;

        this.changed();
	}

    add(item) {
        let date = moment().startOf('month').add('month', 1);
        let itm = {index: item, dateStart: date.format('YYYY-MM-DD'), dateEnd: '', color: '#90EE90'};
        this.service.addMappedIndex(itm);
        this.service.selectMappedIndex(itm);
    }

    changed() {
        this.search(this.searchText);
    }

    search(term: string) {
        this.service.searchIndexes(term);
    }
}