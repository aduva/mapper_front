import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MappingService} from './mapping.service';
import {MappingStore} from './mapping.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';

import * as moment from 'moment/moment';
import 'moment/locale/ru';

@Component({
	moduleId: module.id,
	selector: 'add-mapping',
	templateUrl: 'add-mapping.component.html'
})
export class AddMappingComponent {
	title: string;

	constructor(private service: MappingService) {
	}

	save() {
		if (this.title) {
			let item = {id: this.service.mappingsLength + 1, title: this.title, mapped_indexes: []};
			this.service.addNewMapping(item);
			this.service.selectMapping(item);
			this.cancel();
		}
	}

	cancel() {
		this.title = null;
		this.service.setAddMapping(false);
	}
}