import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Component, OnInit, Input} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MappingService} from './mapping.service';
import {MappingStore} from './mapping.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

import * as moment from 'moment/moment';
import 'moment/locale/ru';

declare var inputmask;

@Component({
	moduleId: module.id,
	selector: 'selected-mapping',
	templateUrl: 'selected-mapping.component.html'
})
export class SelectedMappingComponent implements OnInit {
	selectedMappedIndex: Observable<any>;
	mappedIndexes: Observable<Array<any>>;
	searchText: string = '';
	currentMonth: string;
	months: string[];
	offset: number = -2;
	@Input() item: any;

	colors = ['#f39c12', '#00c0ef', '#0073b7', '#111111', '#dd4b39', '#3c8dbc', '#00a65a', '#d2d6de', '#001f3f', '#39cccc', '#3d9970', '#01ff70', '#ff851b', '#f012be', '#605ca8', '#d81b60'];

	constructor(private service: MappingService,
	            private router: Router,
	            private store: Store<MappingStore>) {

		this.selectedMappedIndex = service.selectedMappedIndex;
		this.mappedIndexes = service.mappedIndexes;

		moment.locale('ru');
	}

	ngOnInit() {
		this.generateMonths(0);
		this.currentMonth = moment().format('MYY');
	}

	selectMappedIndex(mappedIndex) {
		this.service.selectMappedIndex(mappedIndex);
	}

	updateMappedIndex(mappedIndex) {
		this.service.updateMappedIndex(mappedIndex);
	}

	mappingExpired(item) {
		if (!item.dateEnd || item.dateEnd === '')
			return false;

		return true;
	}

	mappingMaps(month, mapping, index) {
		if (this.mappingActiveOnMonth(month, mapping)) {
			return mapping.color;
		}

		return null;
	}

	mappingActiveOnMonth(month, mapping) {
		if (!mapping) {
			return false;
		}

		if (!mapping.dateEnd) {
			return moment(month.date).isSameOrAfter(mapping.dateStart);
		}

		return moment(month.date).isSameOrAfter(mapping.dateStart) && moment(month.date).isSameOrBefore(mapping.dateEnd);
	}

	generateMonths(val) {
		this.offset += val;
		this.months = Array(12).fill()
			.map((x, i) => 11 - i + this.offset)
			.map(i => moment().startOf('month').subtract(i, 'months'))
			.map(month => ({ name: month.format('MMMM YYYY'), date: month.format('YYYY-MM-DD'), month: month.format('MYY') }));
	}

	delete() {
		this.service.deleteMapping(this.item);
	}

	deleteMappedIndexes() {
		this.service.updateMapping(Object.assign({}, this.item, {mapped_indexes: []}));
		this.selectMappedIndex(undefined);
	}
}
