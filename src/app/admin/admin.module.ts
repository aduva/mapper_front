import {AdminComponent} from './admin.component';
import {AdminRouterModule} from './admin.router';
import {BrowserModule} from '@angular/platform-browser';
import {COMPONENTS, SERVICES, STORES} from './components';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, XSRFStrategy, CookieXSRFStrategy, Http, ConnectionBackend, XHRBackend, RequestOptions} from '@angular/http';
import {LocationStrategy, PathLocationStrategy, CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Router} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {StoreLogMonitorModule, useLogMonitor} from '@ngrx/store-log-monitor';

import {DatepickerModule} from '../shared/datepicker/datepicker.component';
import {ColorpickerModule} from '../shared/colorpicker/colorpicker.component';
import {ModalModule} from 'ng2-bootstrap';
import {Select2Module} from 'ng2-select2';

@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        AdminRouterModule,
        DatepickerModule,
        ColorpickerModule,
        Select2Module,
        ModalModule.forRoot(),
        StoreModule.provideStore(STORES),
        StoreDevtoolsModule.instrumentStore({
            monitor: useLogMonitor({
                visible: false,
                position: 'right'
            })
        }),
        StoreLogMonitorModule
    ],
    declarations: [
        AdminComponent,
        COMPONENTS
    ],
    providers: [
        SERVICES
    ],
    bootstrap: [AdminComponent]
})
export class AdminModule {
}
