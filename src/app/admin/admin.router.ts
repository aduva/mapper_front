import {NgModule}     from '@angular/core';
import {RouterModule} from '@angular/router';

import {AdminComponent, AdminHomeComponent} from './admin.component';

import {MappingComponent} from './mapping/mapping.component';
import {ReportsComponent} from './reports/reports.component';
import {ReportsDetailsComponent} from './reports/reports-details.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: AdminComponent,
        children: [{
          path: '',
          children: [
            {path: '', component: AdminHomeComponent},
            {path: 'mapping', component: MappingComponent},
            {path: 'reports', component: ReportsComponent},
            {path: 'reports/:id', component: ReportsDetailsComponent},
          ]
        }]
      }
    ])
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AdminRouterModule {}
