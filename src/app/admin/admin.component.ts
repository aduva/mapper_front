import {Component} from '@angular/core';

@Component({
	moduleId: module.id,
	template: '<router-outlet></router-outlet>'
})
export class AdminComponent {
}

@Component({
    moduleId: module.id,
    selector: 'admin-home',
    templateUrl: 'admin.component.html'
})
export class AdminHomeComponent {
}
