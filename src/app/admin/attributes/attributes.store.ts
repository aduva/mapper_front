export interface AttributesStore {
	selectedAttribute: any;
	attributes: any[];
	values: any[];
};

export const selectedAttribute = (state: any = null, {type, payload}) => {
	switch (type) {
		case 'SELECT_ATTRIBUTE':
			return payload;
		default:
			return state;
	}
};

export const attributes = (state: any = [], {type, payload}) => {
  	switch (type) {
  		case 'ADD_ATTRIBUTES':
	    	return payload;
	    case 'ADD_ATTRIBUTE':
	    	return [...state, payload];
	    case 'UPDATE_ATTRIBUTE':
	      	return state.map(item => {
	      		return item.id === payload.id ? Object.assign(item, payload) : item;
	      	});
	    case 'DELETE_ATTRIBUTE':
	      	return state.filter(item => {
	        	return item.id !== payload.id;
	      	});
	    default:
	      	return state;
  	}
};

export const values = (state: any = [], {type, payload}) => {
  	switch (type) {
  		case 'ADD_VALUES':
	    	return payload;
	    case 'ADD_VALUE':
	    	return [...state, payload];
	    case 'UPDATE_VALUE':
	      	return state.map(item => {
	      		return item.id === payload.id ? Object.assign(item, payload) : item;
	      	});
	    case 'DELETE_VALUE':
	      	return state.filter(item => {
	        	return item.id !== payload.id;
	      	});
	    default:
	      	return state;
  	}
};