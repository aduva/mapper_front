import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Http, Headers}    from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AttributesStore} from './attributes.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class AttributesService {
	items: Observable<Array<any>>;
    url: string = 'api/attributes/';
    private searchTermStream = new Subject<string>();

    constructor(private http: Http,
                private store: Store<AttributesStore>) {
    	this.items = store.select('attributes');
    	this.searchTermStream
			.debounceTime(300)
			.distinctUntilChanged()
			.subscribe(term => this.fetch(term));
    }

    fetch(term: string) {
    	this.http.get(this.url)
		    .map(res => res.json().data)
		    .map(payload => ({ type: 'ADD_ATTRIBUTES', payload: payload }))
		    .subscribe(action => this.store.dispatch(action));
    }

    fetchOne(id) {
    	this.http.get(this.url + id)
		    .map(res => res.json().data)
		    .map(payload => ({ attribute: { type: 'SELECT_ATTRIBUTE', payload: payload }, values: {type: 'ADD_VALUES', payload: payload.values} }))
		    .subscribe(actions => { this.store.dispatch(actions.attribute); this.store.dispatch(actions.values); });
    }

    search(term: string) {
    	this.searchTermStream.next(term);
    }
}