import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import {ActivatedRoute, Router} from '@angular/router';
import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AttributesService} from './attributes.service';
import {AttributesStore} from './attributes.store';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

@Component({
	moduleId: module.id,
	selector: 'attributes-details',
	templateUrl: 'attributes-details.component.html'
})
export class AttributesDetailsComponent implements OnInit {

	item: Observable<any>;
	values: Observable<Array<any>>;

	constructor(private service: AttributesService,
	            private store: Store<AttributesStore>,
	            private route: ActivatedRoute) {
		this.item = store.select('selectedAttribute');
		this.values = store.select('values');
	}

	ngOnInit() {
		this.item.subscribe(i => {
			if (!i) {
				let id = +this.route.snapshot.params['id'];
				this.service.fetchOne(id);
			}
		});
	}
}