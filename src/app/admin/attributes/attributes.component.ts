import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AttributesService} from './attributes.service';
import {AttributesStore} from './attributes.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

@Component({
	moduleId: module.id,
	selector: 'attributes',
	templateUrl: 'attributes.component.html'
})
export class AttributesComponent implements OnInit {
	items: Observable<Array<any>>;
	searchText: string = '';

	constructor(private service: AttributesService,
	            private router: Router,
	            private store: Store<AttributesStore>) {
		this.items = service.items;
	}

	ngOnInit() {
		this.items.subscribe(i => {
			if (!i || i.length === 0) {
				this.service.fetch('');
			}
		});
	}

	changed() {
		this.search(this.searchText);
	}

	search(term: string) {
		this.service.search(term);
	}

	select(item) {
		this.store.dispatch({type: 'SELECT_ATTRIBUTE', payload: item});
		this.store.dispatch({type: 'ADD_VALUES', payload: item.values});
		this.router.navigate(['attributes', item.id]);
	}
}
