export interface ReportsStore {
	selectedReport: any;
	reports: any[];
	indexes: any[];
};

export const selectedReport = (state: any = null, {type, payload}) => {
	switch (type) {
		case 'SELECT_REPORT':
			return payload;
		default:
			return state;
	}
};

export const reports = (state: any = [], {type, payload}) => {
  	switch (type) {
  		case 'ADD_REPORTS':
	    	return payload;
	    case 'ADD_REPORT':
	    	return [...state, payload];
	    case 'UPDATE_REPORT':
	      	return state.map(item => {
	      		return item.id === payload.id ? Object.assign(item, payload) : item;
	      	});
	    case 'DELETE_REPORT':
	      	return state.filter(item => {
	        	return item.id !== payload.id;
	      	});
	    default:
	      	return state;
  	}
};

export const indexes = (state: any = [], {type, payload}) => {
  	switch (type) {
  		case 'ADD_INDEXES':
	    	return payload;
	    default:
	      	return state;
  	}
};