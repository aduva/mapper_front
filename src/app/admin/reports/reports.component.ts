import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ReportsService} from './reports.service';
import {ReportsStore} from './reports.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

@Component({
	moduleId: module.id,
	selector: 'reports',
	templateUrl: 'reports.component.html'
})
export class ReportsComponent implements OnInit {
	items: Observable<Array<any>>;
	searchText: string = '';

	constructor(private service: ReportsService,
	            private router: Router,
	            private store: Store<ReportsStore>) {
		this.items = service.items;
	}

	ngOnInit() {
		this.items.subscribe(i => {
			if (!i || i.length === 0) {
				this.service.fetch('');
			}
		});
	}

	changed() {
		this.search(this.searchText);
	}

	search(term: string) {
		this.service.search(term);
	}

	select(item) {
		this.store.dispatch({type: 'SELECT_REPORT', payload: item});
		this.router.navigate(['reports', item.id]);
	}
}
