import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Http, Headers} from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ReportsStore} from './reports.store';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class ReportsService {
    items: Observable<Array<any>>;
	indexes: Observable<Array<any>>;
    item: Observable<any>;
    url: string = 'api/reports/';
    private searchTermStream = new Subject<string>();

    constructor(private http: Http,
                private store: Store<ReportsStore>) {
        this.items = store.select('reports');
    	this.indexes = store.select('indexes');
        this.item = store.select('selectedReport');
    	this.searchTermStream
			.debounceTime(300)
			.distinctUntilChanged()
			.subscribe(term => this.fetch(term));
    }

    fetch(term: string) {
    	this.http.get(this.url + '?title=^' + term)
		    .map(res => res.json().data)
		    .map(payload => ({ type: 'ADD_REPORTS', payload: payload }))
		    .subscribe(action => this.store.dispatch(action))
        ;
    }

    fetchIndexes(term) {
        this.http.get('/api/indexes?term=' + term)
            .map(res => res.json().data.content)
            .map(payload => ({ type: 'ADD_INDEXES', payload: payload }))
            .subscribe(action => this.store.dispatch(action))
        ;
    }

    fetchOne(id) {
    	this.http.get(this.url + id)
		    .map(res => res.json().data)
		    .map(payload => ({ type: 'SELECT_REPORT', payload: payload }))
		    .subscribe(action => this.store.dispatch(action))
        ;
    }

    search(term: string) {
    	this.searchTermStream.next(term);
    }

    select(item) {
        this.store.dispatch({type: 'SELECT_REPORT', payload: item});
        this.fetchIndexes(item.id);
    }
}