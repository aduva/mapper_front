import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import {ActivatedRoute, Router} from '@angular/router';
import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ReportsService} from './reports.service';
import {ReportsStore} from './reports.store';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs/Subject';

@Component({
	moduleId: module.id,
	selector: 'reports-details',
	templateUrl: 'reports-details.component.html'
})
export class ReportsDetailsComponent implements OnInit {

	item: Observable<any>;
	values: Observable<Array<any>>;

	constructor(private service: ReportsService,
	            private store: Store<ReportsStore>,
	            private route: ActivatedRoute) {
		this.item = store.select('selectedReport');
	}

	ngOnInit() {
		this.item.subscribe(i => {
			if (!i) {
				let id = +this.route.snapshot.params['id'];
				this.service.fetchOne(id);
			}
		});
	}
}