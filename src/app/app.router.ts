import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {path: '', loadChildren: 'app/admin/admin.module#AdminModule' }
    ])
  ],
  exports: [
    RouterModule
  ]
})

export class AppRouterModule {}
